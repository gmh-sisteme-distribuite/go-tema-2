package main

import (
	"fmt"
	"sync"
	"unicode"
)

/*
6.  Se dă un vector de vectori, care conțin mai multe cuvinte ce pot avea in componența lor doar litere.
Să se afle numărul mediu de cuvinte care  încep și se termina cu litera mare
și au un număr par de litere mici folosind tehnica map - reduce.

De exemplu:

input := [][]string{
{"AcasA", "CasA", "FacultatE", "SisTemE", "distribuite"},
{"GolanG", "map", "reduce", "Problema", "TemA","ProieCt"},
{"LicentA", "semestru", "ALGORitM", "StuDent"}} se va afisa 1.66
*/

// mapper receives a channel of strings and counts the occurrence of each unique word read from this channel. It sends the resulting map to the output channel.
func mapper(in <-chan string, out chan<- map[string]int) {
	count := map[string]int{}
	for word := range in {
		conditionsPassed := validateString(word)

		if conditionsPassed {
			count["pass"]++
		} else {
			count["fail"]++
		}
	}
	out <- count
	close(out)
}

func validateString(word string) bool {
	uppercaseConditions := hasFirstAndLastLettersUppercase(word)
	evenLowercaseLettersCondition := hasEvenLowercaseLetters(word)

	return uppercaseConditions && evenLowercaseLettersCondition
}

func hasEvenLowercaseLetters(word string) bool {
	lowercaseRunesCount := countLowerCaseRunes(word)

	return lowercaseRunesCount % 2 == 0
}

func countLowerCaseRunes(word string) int {
	count := 0
	for _, char := range word {
		if unicode.IsLower(char) {
			count++
		}
	}

	return count
}

func hasFirstAndLastLettersUppercase(word string) bool {
	firstLetterUppercase := false
	lastLetterUppercase := false
	wordLength := len(word)
	lastLetterIndex := wordLength - 1

	for index, char := range word {
		if index == 0 && unicode.IsUpper(char) {
			firstLetterUppercase = true
		} else if index == lastLetterIndex && unicode.IsUpper(char) {
			lastLetterUppercase = true
		}
	}

	return firstLetterUppercase && lastLetterUppercase
}

// reducer receives a channel of ints and adds up all ints until the channel is closed. Then it divides through the number of received ints to calculate the average.
func reducer(in <-chan int, out chan<- float32) {
	sum, count := 0, 0
	for n := range in {
		sum += n
		count++
	}
	out <- float32(sum) / float32(count)
	close(out)
}

// inputDistributor receives three output channels and sends each of them some input.
func inputReader(out [3]chan<- string) {
	// Example input from the problem
	input := [][]string{
		{"AcasA", "CasA", "FacultatE", "SisTemE", "distribuite"},
		{"GolanG", "map", "reduce", "Problema", "TemA","ProieCt"},
		{"LicentA", "semestru", "ALGORitM", "StuDent"},
	}

	for i := range out {
		go func(ch chan<- string, word []string) {
			for _, w := range word {
				ch <- w
			}
			close(ch)
		}(out[i], input[i])
	}
}

// shuffler gets a list of input channels containing key/value pairs like
// "noun: 5, verb: 4". For each "noun" key, it sends the corresponding value
// to out[0], and for each "verb" key to out[1].
// The input channles are multiplexed into one, based on the `merge` function
// from the [Pipelines article](https://blog.golang.org/pipelines) of the
// Go Blog.
func shuffler(in []<-chan map[string]int, out [2]chan<- int) {
	var wg sync.WaitGroup
	wg.Add(len(in))
	for _, ch := range in {
		go func(c <-chan map[string]int) {
			for m := range c {
				nc, ok := m["pass"]
				if ok {
					out[0] <- nc
				}
				vc, ok := m["fail"]
				if ok {
					out[1] <- vc
				}
			}
			wg.Done()
		}(ch)
	}
	go func() {
		wg.Wait()
		close(out[0])
		close(out[1])
	}()
}

// outputWriter starts a goroutine for each input channel and writes out
// the averages that it receives from each channel.
func outputWriter(in []<-chan float32) {
	var wg sync.WaitGroup
	wg.Add(len(in))
	// `out[0]` contains the average number of strings that passed the validation, `out[1]` the average number of strings that failed the validation.
	name := []string{"pass", "fail"}
	for i := 0; i < len(in); i++ {
		go func(n int, c <-chan float32) {
			for avg := range c {
				fmt.Printf("Average number of strings that %sed the validation: %f\n", name[n], avg)
			}
			wg.Done()
		}(i, in[i])
	}
	wg.Wait()
}

func main() {
	// Set up all channels used for passing data between the workers.
	//
	// This is quite silly; I could have used loops instead, to create arrays or
	// slices of channels. Apevenently, copy/paste has won.
	size := 10
	text1 := make(chan string, size)
	text2 := make(chan string, size)
	text3 := make(chan string, size)

	map1 := make(chan map[string]int, size)
	map2 := make(chan map[string]int, size)
	map3 := make(chan map[string]int, size)
	reduce1 := make(chan int, size)
	reduce2 := make(chan int, size)
	avg1 := make(chan float32, size)
	avg2 := make(chan float32, size)

	// Start all workers in seevenate goroutines, chained together via channels.
	go inputReader([3]chan<- string{text1, text2, text3})
	go mapper(text1, map1)
	go mapper(text2, map2)
	go mapper(text3, map3)
	go shuffler([]<-chan map[string]int{map1, map2, map3}, [2]chan<- int{reduce1, reduce2})
	go reducer(reduce1, avg1)
	go reducer(reduce2, avg2)

	// The outputWriter runs in the main thread.
	outputWriter([]<-chan float32{avg1, avg2})
}
